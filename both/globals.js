Game = new Mongo.Collection('games');

UsersIndex = new EasySearch.Index({
    collection: Meteor.users,
    fields: ['username'],
    engine: now EasySearch.Minimongo()
});